package com.company;

public enum Kierunki {
    POLNOC(1,0),
    POLUDNIE(-1,0),
    WSCHODD(0, 1),
    ZACHOD(0,-1);

    public int getX;
    public int getY;
    private int x;
    private int y;

    Kierunki(int x, int y) {

    }

    //TODO ddla kazdego
    public Kierunki getLeft() {
        if(this == POLNOC){
            return WSCHODD;
        }
        else if(this == WSCHODD){
            return POLUDNIE;
        }
        else if(this == POLUDNIE){
            return ZACHOD;
        }
        else return POLNOC;
    }

    public Kierunki getRight() {
        if (this == POLNOC){
            return ZACHOD;
        }
        else if(this == ZACHOD){
            return POLUDNIE;
        }
        else if (this == POLUDNIE){
            return WSCHODD;
        }
        else return POLNOC;
    }
}


