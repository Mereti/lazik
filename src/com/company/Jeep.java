package com.company;


import java.awt.*;
import java.awt.geom.Point2D;


public class Jeep implements GoGoNasa{

    private Kierunki kierunek;
    private int speed = 1;
    final int MIN_X = 0;
    final int MIN_Y = 0;
    final int MAX_X = 10;
    final int MAX_Y = 10;


    private int [][] myTable = new int[MAX_X][MAX_Y] ;
    private Point startPoint = new Point(0,0)  ;


    @Override
    public void goForvard(int speed) {
    //TODO przemnozyc przez kierunek
    int ileDodacDoX = speed * kierunek.getX;
    int ileDodacDoY = speed * kierunek.getY;

        this.speed = speed;
        if(startPoint.x+ileDodacDoX <= MAX_X && startPoint.x+ileDodacDoX >= MIN_X){
            int z = this.startPoint.x+ileDodacDoX;
            startPoint.move(z,startPoint.y);
        }else System.out.println("Error -> end of table -> turn left or right");

        if(startPoint.y+ileDodacDoY <= MAX_Y && startPoint.y+ileDodacDoY >= MIN_Y){
            int z = this.startPoint.y+ileDodacDoY;
            startPoint.move(startPoint.x,z);
        }else System.out.println("Error -> end of table -> turn left or right");
    }

    @Override
    public void goBack(int speed) {

        goForvard(speed * -1);

        /*this.speed = speed;
        if(startPoint.x-speed >= MIN_X){
            int z = this.startPoint.x-speed;
            this.startPoint.move(z,this.startPoint.y);
        }else System.out.println("Error -> end of table -> turn left or right");*/
    }



    @Override
    public void stop() {


    }

    @Override
    public void rotation(boolean right) {
        if(right) kierunek = kierunek.getRight();
        else kierunek = kierunek.getLeft();

    }

    @Override
    public void position(Point lokalizacja) {
        System.out.println(this.startPoint.getLocation());

    }
}


