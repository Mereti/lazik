package com.company;

import javax.swing.text.Position;
import java.awt.*;


public interface GoGoNasa {



    public void goForvard(int speed);
    public void goBack(int speed);
    public void stop();
    public void rotation(boolean right);
    public void position(Point lokalizacja);


}
